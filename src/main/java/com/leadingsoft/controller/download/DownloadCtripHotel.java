package com.leadingsoft.controller.download;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.cef.browser.CefBrowser;
import org.cef.callback.CefStringVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.leadingsoft.common.Constant;
import com.leadingsoft.common.TaskQuene;

public class DownloadCtripHotel implements CefStringVisitor {
	private static Logger logger = LoggerFactory.getLogger(DownloadCtripHotel.class);
	private CefBrowser browser;
	private Long startIndex;
	private Long stopIndex;

	public DownloadCtripHotel(CefBrowser browser, Long startIndex, Long stopIndex) {
		this.browser = browser;
		this.startIndex = startIndex;
		this.stopIndex = stopIndex;
	}
	@Override
	public void visit(String string) {
		String url = browser.getURL();
		// 下载文件保存位置
		StringBuilder filePath = new StringBuilder("/home/ctrip/");
		String index = StringUtils.remove(url, Constant.CTRIPHOTELLISTUTLTEMP);
		filePath.append(index).append(".txt");
		File file = new File(filePath.toString());
		try {
			if (!file.exists() && StringUtils.isNoneBlank(string)) {
				// 开始下载文件
				FileUtils.writeStringToFile(file, string, "UTF-8", false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			String nextUrl = TaskQuene.getCtripHotelListUrl(url, startIndex, stopIndex);
			if (StringUtils.isNotEmpty(nextUrl)) {
				TaskQuene.ctripTaskUrl.add(nextUrl.toString());
				logger.debug("获取到下一页URL：{}", nextUrl);
				logger.debug("当前队列中的URL为：{}", TaskQuene.ctripTaskUrl);
			} else {
				// 当前任务结束。关闭浏览器
				logger.debug("未获取到下一页URL，抓取任务结束");
				browser.close();
			}
		}
	}
}
