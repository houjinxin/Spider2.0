/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title: package-info.java 
 * @Prject: Spider2.0
 * @Package: com.leadingsoft.controller.other.impl 
 * @Description: 新版爬虫，使用Java调用浏览器引擎
 * @author: gongym   
 * @date: 2018年6月21日 下午12:18:38 
 * @version: V1.0   
 */
/**
 * @ClassName: package-info
 * @Description: Jsoup引擎实现类
 * @author: gongym
 * @date: 2018年6月21日 下午12:18:38
 */
package com.leadingsoft.controller.other.impl;
