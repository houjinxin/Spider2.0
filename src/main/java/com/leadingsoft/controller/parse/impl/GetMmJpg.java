package com.leadingsoft.controller.parse.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.leadingsoft.common.Constant;
import com.leadingsoft.common.model.MmJpg;
import com.leadingsoft.controller.parse.GetDocumentData;

/**
 * @ClassName GetMmJpg
 * @Description 解析列表页获取妹子ID
 * @author gongym
 * @date 2018年6月22日 上午12:19:10
 */
public class GetMmJpg implements GetDocumentData {
	@Override
	public Object elementToObject(Element element) {
		return null;
	}
	@Override
	public List<Object> elementsToObjects(Elements elements) {
		// 用这个方法进行解析
		List<Object> mmList = new ArrayList<Object>();
		elements.forEach((document) -> {
			MmJpg mmJpg = new MmJpg();
			Elements allElement = document.select("a");
			if (null != allElement && allElement.size() > 0) {
				String mmUrl = allElement.get(0).attr("href");
				String mmName = allElement.get(0).text();
				String mmId = StringUtils.remove(mmUrl, Constant.MMJPGMMLISTURLTEMP);
				mmJpg.setMmId(mmId);
				mmJpg.setMmUrl(mmUrl);
				mmJpg.setMmName(mmName);
			}
			mmList.add(mmJpg);
		});
		return mmList;
	}
}
