package com.leadingsoft.test;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.apache.commons.configuration.Configuration;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.ConnectionSource;
import org.beetl.sql.core.ConnectionSourceHelper;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.cef.CefApp;
import org.cef.CefApp.CefAppState;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefAppHandlerAdapter;
import org.cef.handler.CefLoadHandlerAdapter;

import com.leadingsoft.core.AddLibraryDir;
import com.leadingsoft.core.CommonConfig;

public class TestMain extends JFrame {
	private static final long serialVersionUID = -5570653778104813836L;

	@SuppressWarnings("unused")
	private TestMain(String startURL, boolean useOSR, boolean isTransparent) {
		CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
			@Override
			public void stateHasChanged(org.cef.CefApp.CefAppState state) {
				if (state == CefAppState.TERMINATED)
					System.exit(0);
			}
		});
		CefSettings settings = new CefSettings();
		settings.windowless_rendering_enabled = useOSR;
		CefApp cefApp = CefApp.getInstance(settings);
		CefClient client = cefApp.createClient();
		CefBrowser browser = client.createBrowser(startURL, useOSR, isTransparent);
		Component component = browser.getUIComponent();
		JTextField address = new JTextField(startURL, 100);
		address.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.loadURL(address.getText());
			}
		});
		getContentPane().add(address, BorderLayout.NORTH);
		getContentPane().add(component, BorderLayout.CENTER);
		pack();
		setSize(800, 600);
		setVisible(true);
		Configuration config = CommonConfig.getInstance();
		String datasourceDriver = config.getString("datasource.driver");
		String datasourceUrl = config.getString("datasource.url");
		String datasourceUsername = config.getString("datasource.username");
		String datasourcePassword = config.getString("datasource.password");
		ConnectionSource source = ConnectionSourceHelper.getSimple(datasourceDriver, datasourceUrl, datasourceUsername,
				datasourcePassword);
		DBStyle mysql = new MySqlStyle();
		SQLLoader loader = new ClasspathLoader("/");
		UnderlinedNameConversion nc = new UnderlinedNameConversion();
		SQLManager sqlManager = new SQLManager(mysql, loader, source, nc, new Interceptor[] { new DebugInterceptor() });
		client.addLoadHandler(new CefLoadHandlerAdapter() {
			@Override
			public void onLoadEnd(CefBrowser browser, CefFrame frame, int arg2) {
				System.out.println(frame.getIdentifier());
				System.out.println(frame.isFocused());
				System.out.println(frame.isMain());
				System.out.println(frame.isValid());
				System.out.println(frame.getName());
				System.out.println(frame.getURL());
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				CefApp.getInstance().dispose();
				dispose();
			}
		});
	}
	public static void main(String[] args) {
		// 添加DLL库
		AddLibraryDir.addDLL();
		new TestMain("http://hotels.ctrip.com/hotel/beijing1/p1", OS.isLinux(), false);
	}
}
