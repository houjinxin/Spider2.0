package com.leadingsoft.core;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class CommonConfig {
	private static Configuration config;
	private static Lock lock = new ReentrantLock(true);

	public static Configuration getInstance() {
		if (config == null) {
			try {
				lock.lock();
				if (config == null)
					config = new PropertiesConfiguration("application.properties");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
		return config;
	}
}
