package com.leadingsoft.common;

public class Constant {
	// DLL库地址
	public static final String DLLPATH = "D:/Products/smallSplit/new/jcef/binary_distrib/win64/bin/lib/win64";
	// SLEEP时间，保证每一个页面在网络不顺畅时都可以正常加载完毕
	public static final Long SLEEP = 5 * 1000L;
	// 默认访问页面
	// 顺便验证网络是否通畅
	public static final String DEFAULTURL = "http://www.baidu.com";
	// 携程酒店列表URL模板（北京地区）
	public static final String CTRIPHOTELLISTUTLTEMP = "http://hotels.ctrip.com/hotel/beijing1/p";
	// 携程酒店评论列表URL模板（北京地区）
	public static final String CTRIPHOTELCOMMENTURLTEMP = "http://hotels.ctrip.com/hotel/dianping/";
	// 每页评论数
	public static final Integer EVERYPAGECOMMENTNUM = 15;
	// 网易云URL
	public static final String MUSIC163URL = "https://music.163.com/#/user/fans?id=109284159";
	// 第一页：http://www.mmjpg.com/
	public static final String MMJPGLISTURLTEMP = "http://www.mmjpg.com/";
	// 第一页：http://www.mmjpg.com/mm/1375
	public static final String MMJPGMMLISTURLTEMP = "http://www.mmjpg.com/mm/";
	// http://fm.shiyunjj.com/
	public static final String MMJPGIMGURLTEMP = "http://fm.shiyunjj.com/";
	// 图片保存位置+妹子ID+图片名称
	public static final String MMJPGSAVEPATH = "/home/mmjpg/";
}
