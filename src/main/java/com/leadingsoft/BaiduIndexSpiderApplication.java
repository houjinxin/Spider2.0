package com.leadingsoft;

import org.cef.CefApp;
import org.cef.CefApp.CefAppState;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;
import org.cef.handler.CefAppHandlerAdapter;

import com.leadingsoft.controller.browser.impl.BaiduIndexBrowserInstance;
import com.leadingsoft.core.AddLibraryDir;

public class BaiduIndexSpiderApplication {
	public static void main(String[] args) {
		CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
			@Override
			public void stateHasChanged(org.cef.CefApp.CefAppState state) {
				if (state == CefAppState.TERMINATED)
					System.exit(0);
			}
		});
		AddLibraryDir.addDLL();
		boolean osrEnabledArg = OS.isLinux();
		boolean transparentPaintingEnabledArg = false;
		CefSettings settings = new CefSettings();
		settings.windowless_rendering_enabled = osrEnabledArg;
		settings.background_color = settings.new ColorType(100, 255, 242, 211);
		CefApp myApp = CefApp.getInstance(settings);
		final CefClient client = myApp.createClient();
		String cookiePath = "D:\\Tmp\\cookies.txt";
		// 新建一个浏览器实例进行抓取
		BaiduIndexBrowserInstance baiduIndexBrowserInstance = new BaiduIndexBrowserInstance(client, osrEnabledArg,
				transparentPaintingEnabledArg, cookiePath);
		baiduIndexBrowserInstance.createBrowserAndCrawler(1L, 2L);
	}
}
